<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MembershipController extends Controller
{
    public function index() {
        return view('membershippage.aboutmembership');
    }

    public function perintis() {
        return view('membershippage.perintismembership');
    }

    public function pengembang() {
        return view('membershippage.pengembangmembership');
    }

    public function detailperintis() {
        return view('membershippage.daftarmembership.detailmembership');
    }

    public function detailpengembang() {
        return view('membershippage.daftarmembership.detailmembership');
    }
}
