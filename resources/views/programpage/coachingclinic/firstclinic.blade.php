@extends('template_merintis.merintis')

@section('head-title', 'Coaching Clinic | Merintis Indonesia')

@section('meta-description')
<meta name="description" content="Merintis Indonesia - Merintis Indonesia adalah ekosistem kreatif muda/i daerah untuk saling terhubung, berkolaborasi, dan melahirkan bisnis-bisnis yang inovatif, solutif dan aplikatif dari proses hulu ke hilir.">
@endsection

@section('addCSS')
<link rel="stylesheet" href="{{ asset('assets/css/home.css') }}">
@endsection

<?php
  $isLogin = false;
  $idAkun = '';
  if (Auth::check()) {
    $isLogin = true;
  } else if (Auth::viaRemember()) {
    $isLogin = true;
  }
?>

@section('header')
<div class="mt-3">
  <header>
     {{-- BATAS NAVBAR  --}}
    <nav class="navbar navbar-expand-sm navbar-dark">
      <div class="mx-auto">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMis" aria-controls="navbarMis" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="navbarMis">
        <ul class="navbar-nav mx-auto fixed">
          <li class="nav-item">
            <a class="nav-link" href="/#beranda">Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#tentang">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#program">Program</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#team">Team</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#content">Content</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://info-merintisindonesia.medium.com/">Blog</a>
          </li>
          <li class="nav-item">
             {{-- Login/Logout  --}}
            <div id="link-log">
              <?= ($isLogin ? '<a class="nav-link" href="javascript:void(0)" onclick="logout()">Sign Out</a>' : '<a class="nav-link" id="btnSignIn" href="'.url('/signin').'">Sign In</a>'); ?>
            </div>
             {{-- End Login/Logout  --}}
          </li>
        </ul>
      </div>
    </nav>
  </header>
  {{-- ================ END Header Menu Area ================= --}}
  @endsection

  @section('content')
  {{-- ================ FIXED ALERT ================= --}}
  <div class='fixed-alert'></div>
  {{-- ================ END FIXED ALERT ================= --}}

</div>
{{-- ============== SECTION PROGRAM  --}}
<section id="program" class="mt-4">
  <!-- <h1 class="roboto-condensed text-center text-green mt-64">Coaching Clinic</h1> -->
  <div class="container">
      <div class="menu-program mt-5" align="center">          
          <div class="hidden form-group col-md-12">
            <figure class="px-48">
              <img src="{{ asset('assets/img/logo-mis-color.png') }}" alt="logo-mis" class="img-100 mb-4">
              <figcaption class="text-18 let-space-08 text-center mt-2" style="width:100%;">
                <p><b> Merintis Indonesia Coaching  Clinic </b>merupakan media konsultasi dan mentoring secara privat (1-1) maupun grup (1-Group) mengenai masalah dalam perkembangan bisnismu dan menjadi sarana menemukan solusi yang tepat dengan level problem, didampingi dengan praktisi bisnis yang akan dilaksanakan secara online</p>
              </figcaption>
            </figure>
          </div>
           <button type="button" class="btn btn-kuning mb-1" style="width:200px; "><a style="color:#fff;" href="/program/daftar/coachclinic/1-on-1/{$id}">Register</a></button>
           <button type="button" id="mentor" class="btn btn-kuning" style="width:200px; background-color:#4ab44f;"><a style="color:#fff;" href="">List Mentor</a></button>
          
           
          <h3 class="roboto-condensed text-center text-green mt-64">Join Upcoming Miniclass</h3>
      </div>        
      <!-- DATA PROGRAM -->
      <div id="data-program" class="mt-32 mb-4">
          <!-- ON LOADING -->
          <div class="my-4 d-flex justify-content-center" id="loader">
              <strong class="text-center">Loading...</strong>
              <div class="mx-4 loader"></div>
          </div>

          <div class="d-flex flex-wrap justify-content-center" id="loc-data">
          </div>

          <div id="loc-btn-program" class="text-center mt-4">
          </div>
      </div>
      <!-- END DATA PROGRAM -->
  </div>
</section>


@endsection

@section('footer')
{{-- ================ Footer Area ================= --}}
<footer class="roboto py-2">
  <div class="container text-center">
    <span class="text-grey">Copyright &copy; <span id="yearNow">tahun</span> All rights reserved</span>
  </div>
</footer>
{{-- ================ END Footer Area ================= --}}
@endsection

@section('addScript')

<script>
  // Tambahan Script
  var btn = document.getElementById('mentor');
    btn.addEventListener('click', function() {
      document.location.href = '/program/coachclinic/mentor';
    });

  // ----------- CEK JIKA DARI HALAMAN DAFTAR -----------
  $(document).ready(function() {
    let params = (new URL(document.location)).searchParams;
    let stat = parseInt(params.get('stat'));
    if (stat) {
      $("#modalLogin").modal('toggle');
    }
  });
  // ----------- END CEK JIKA DARI HALAMAN DAFTAR -----------

  // SHOW NAVBAR FIXED
//   window.onscroll = changeNav;

//   function changeNav() {
//     let navbar = $('nav');
//     if (window.pageYOffset > 90) {
//       navbar.addClass('navbar-fixed')
//     } else {
//       navbar.removeClass('navbar-fixed');
//     }
//   }

  // GET YEAR NOW
  let date = new Date();
  let now = date.getFullYear();
  document.getElementById("yearNow").innerHTML = now;

    // ==================== LOGOUT ===================
  function logout() {
    //console.log("Berhasil logout");
    // --------------- LOGOUT -------------------
    $(this).on("click", function() {
      // ------ MENGHAPUS SESSION DI DATABASE
      $('#link-log').addClass("active");
      $.ajax({
        type: "post",
        url: "{{ url('/akun/hapussession') }}",
        data: {"_token": "{{ csrf_token() }}" },
        success: function() {
          window.location.replace("{{ url('/') }}");
        },
        error: function(err) {
          // console.error(err)
        }
      })
      // ------ END MENGHAPUS SESSION DI DATABASE
    })
  }
  // ==================== END LOGOUT ===============

  // ------------------ FIXED ALERT
  function alFixedBerhasil(pesan) {
    let alBerhasil = `
      <div class="alert alert-success alert-dismissible fade show" role="alert">
      <div id="text-berhasil">${pesan}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>
      `
    $('.fixed-alert').html(alBerhasil)
    $('.fixed-alert').html(alBerhasil).animate({
      right: '12px'
    })

    $('.fixed-alert .close').on("click", function() {
      $('.fixed-alert').animate({
        right: '-100px'
      });
      $('.fixed-alert .alert').removeClass('.show');
    })
  }

  function alFixedGagal(pesan) {
    let alGagal = `
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <div id="text-berhasil">${pesan}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>
      `
    $('.fixed-alert').html(alGagal).animate({
      right: '12px'
    })

    $('.fixed-alert .close').on("click", function() {
      $('.fixed-alert').animate({
        right: '-100px'
      });
      $('.fixed-alert .alert').removeClass('.show');
    })
  }
  // -------------------------- END FIXED ALERT

  // ------------------ END BTN COLLAPSE VIP ----------------

  // SHOW MESSAGE SUCCESS LOGIN
      if(sessionStorage.getItem("login") === "berhasil") {
        alFixedBerhasil('Login Berhasil')
        sessionStorage.removeItem("login")
      }
  // END SHOW MESSAGE SUCCESS LOGIN

    // ========== BTN SIGNIN
  $('#btnSignIn').on("click", function() {
    $('#link-log').addClass("active");
  })
  // ========== END BTN SIGNIN

  // ============ PROSES MENGOLAH DATA
  function getPastProg() {
    // Ambil Data PAST PROGRAM
    $.ajax({
        type: "get",
        url: "{{ url('/home/pastprogram/0') }}",
        beforeSend: function() {
                $("#loader").addClass('d-flex');
                $("#loader").removeClass('d-none');
                $("#btnNextProgram").addClass('wait');
                $("#btnNextProgram").attr('disabled', true);
            },
        success: function(data) {
            //console.log(data)
            let dtPast = ''
            $.each(data.pastProg, (index, val) => {
                dtPast +=
                `
                <div class="card-prog border-none">
                    <div class="custom-card">
                        <span class="roboto-condensed">${val.nama_program}</span>
                        <a href="{{ url('/program/daftar/').'/'.$upcoProg[0]['link_daftar'].'/'.'35' }}" onclick="secureIDUpcoming(${val.id})" >
                            <img src="{{ asset('storage/images/programs/') }}/${val.link_pamflet}" loading="lazy" alt="${val.nama_program}" class="img-prog-180 border-gold-mis mx-2 mx-md-4 my-2">
                        </a>
                    </div>
                </div>
                `
            })
            const btnNextprev = `
            <button id="btnPrevProgram" class="btn-prevNext mx-2" onclick="prevProgram(${data.next})" disabled><i class="mr-2 fas fa-chevron-left"></i> Prev</button>
            <button id="btnNextProgram" class="btn-prevNext" onclick="nextProgram(${data.next})">Next <i class="ml-2 fas fa-chevron-right"></i></button>
            `
            $("#btnNextProgram").removeClass('wait');
            $("#btnNextProgram").attr('disabled', false);
            $('#loc-data').html(dtPast)
            $('#loc-btn-program').html(btnNextprev)
            $('#loader').removeClass('d-flex')
            $('#loader').addClass('d-none')
        },
        error: function(err) {
            console.error(err)
        }
    })
  }
  
  // SECURE DETAIL UPCOMING
  function secureIDUpcoming(id) {
    let isLogin = '<?= ($isLogin ? $isLogin: 'false') ?>'
    if(isLogin === 'false') {
      alFixedGagal('Silahkan sign in dahulu!')
    } else {
      window.location.href = `{{ url('/program/coachclinic') }}/35`
    }
  }

  function getOngoingProg() {
    $.ajax({
        type:"get",
        url:"{{ url('/home/ongoingprogram') }}",
        beforeSend: function() {
            $("#loader").addClass('d-flex');
            $("#loader").removeClass('d-none');
        },
        success: function(data) {
            // console.log(data)
            let dtOngo = ''
            $.each(data, function(index, val) {
                let endDate = new Date(val.tgl_selesai)
                let startDate = new Date(val.tgl_mulai)
                startDate = startDate.toDateString()
                endDate = (val.tgl_selesai === null) ? "" : "- "+endDate.toDateString()

                dtOngo +=
                `
                <div class="card-prog border-none m-sm-2 bg-white rounded">
                    <div class="card-body-prog px-1 px-2 py-2 border-gold">
                    <img src="{{ asset('storage/images/programs/') }}/${val.link_pamflet}" alt="${val.nama_program}" class="img-prog-180 mx-1 x-md-4 my-2">
                        <p class="roboto" style="width: 150px;">
                        ${val.nama_program}: <br>
                        <span class="roboto-condensed">${val.nama_kegiatan}</span>
                        </p>
                        <table style="width: 160px; font-size: 14px;">
                            <tr>
                                <td><small>${startDate}</small></td>
                                <td><small>${val.jam_mulai}</small></td>
                            </tr>
                            <tr>
                                <td><small>${endDate}</small></td>
                                <td><small>- ${val.jam_selesai} WIB</small></td>
                            </tr>
                        </table>
                        <p class="roboto-condensed" style="width: 150px; font-size: 12px;">*${val.sasaran_program}</p>
                    </div>
                    <a href="javascript:void(0)" onclick="secureIDProgram(${val.id})" class="btn-kuning link-none text-white d-block mx-auto my-2 text-center">DETAIL</a>
                </div>
                `
            })
            if (data.length < 1) {
                dtOngo = `<span class="text-center mt-3 mx-2">Click <a class="roboto-condensed" href="{{ url('?program=upcoming')}}">Upcoming Program</a> to see our latest programs</span>`
            }
            $('#loc-data').html(dtOngo)
            $('#loc-btn-program').html('')
            $("#loader").removeClass('d-flex');
            $("#loader").addClass('d-none');
        },
        error: function(err) {
            console.error(err)
        }
    })
  }

  function getUpcomProg() {
    $.ajax({
      type:"get",
      url:"{{ url('/home/upcomingprogram') }}",
      beforeSend: function() {
          $("#loader").addClass('d-flex');
          $("#loader").removeClass('d-none');
      },
      success: function(data) {
          // console.log(data)
          let dtUpco = ''
          $.each(data, function(index, val) {
              let endDate = new Date(val.tgl_selesai)
              let startDate = new Date(val.tgl_mulai)
              startDate = startDate.toDateString()
              endDate = (val.tgl_selesai === null) ? "" : "- "+endDate.toDateString()
              let jamTambah = (val.jam_tambahan) ? ` & ${val.jam_tambahan}` : ''
              //console.log(endDate)

              dtUpco +=
                `<div class="card border-none mx-2 my-2">
                    <div class="program-card radius-12px bg-upcoming p-3">
                        <h5 class="roboto-condensed">${val.nama_program}</h5>
                        <h6 class="roboto-condensed">${val.nama_kegiatan}</h6>
                        <b class="text-14 roboto-condensed">Yang akan kamu pelajari</b>
                        ${val.desc_program}
                        <div class="mx-1 mx-sm-2 row roboto-condensed">
                            <div class="col-7">
                              ${startDate} ${endDate}
                            </div>
                            <div class="col-5">
                                Limited Seats
                            </div>
                        </div>
                        <div class="mx-2 row roboto text-11">
                            <div class="col-7">
                                ${val.jam_mulai} - ${val.jam_selesai} ${jamTambah}
                            </div>
                            <div class="col-5">
                              ${val.metode_pelaksanaan}
                            </div>
                        </div>
                    </div>
                    <a href="javascript:void(0)" onclick="secureIDUpcoming(${val.id})" class="btn-kuning link-none text-white d-block mx-auto my-2 text-center">Daftar Sekarang</a>
                </div>
                `
          })
          $('#loc-data').html(dtUpco)
          $('#loc-btn-program').html('')
          $("#loader").removeClass('d-flex')
          $("#loader").addClass('d-none')
      },
      error: function(err) {
          console.error(err)
      }
    })
  }

  // MOVE TAB PROGRAM
  $(document).ready(function() {
      // UPCOMING PROGRAM
      $('#upcoming-program').on("click", function() {
        $('#past-program').removeClass('menu-active')
        $('#ongoing-program').removeClass('menu-active')
        $('#upcoming-program').addClass('menu-active')
        // AMBIL UPCOMING PROGRAM
        getUpcomProg()
      })
    })

  // === AMBIL PARAMETER
  const urlParams = new URLSearchParams(window.location.search)
  const params = urlParams.get('program')
  //console.log(params)
  if (params === 'upcoming') {
    window.location.hash = 'program'
    $('#past-program').removeClass('menu-active')
    $('#ongoing-program').removeClass('menu-active')
    $('#upcoming-program').addClass('menu-active')
    getUpcomProg()
  } else {
    getPastProg()
  }
  // === END AMBIL PARAMETER

  // SECURE DETAIL ONGOING
  function secureIDProgram(id) {
    let isLogin = '<?= ($isLogin ? $isLogin: 'false') ?>'
    if(isLogin === 'false') {
      alFixedGagal('Silahkan sign in dahulu!')
    } else {
      window.location.href = `{{ url('/program/detail/ongoing') }}/${id}`
    }
  }

  // SECURE DETAIL UPCOMING
  function secureIDUpcoming(id) {
    let isLogin = '<?= ($isLogin ? $isLogin: 'false') ?>'
    if(isLogin === 'false') {
      alFixedGagal('Silahkan sign in dahulu!')
    } else {
      window.location.href = `{{ url('/program/detail/upcoming') }}/${id}`
    }
  }

  //BUTTON NEXT PAST PROGRAM
  function nextProgram(nextPage) {
    let page = parseInt(nextPage)+12
    $.ajax({
        type: "get",
        url: `{{ url('/home/pastprogram/${page}') }}`,
        beforeSend: function() {
                $("#loader").addClass('d-flex');
                $("#loader").removeClass('d-none');
                $("#btnNextProgram").addClass('wait');
                $("#btnNextProgram").attr('disabled', true);
            },
        success: function(data) {
            //console.log(data)
            let dtPast = ''
            $.each(data.pastProg, (index, val) => {
                dtPast +=
                `
                <div class="card-prog border-none">
                    <div class="custom-card">
                        <span class="roboto-condensed">${val.nama_program}</span>
                        <a href="{{ url('/program/daftar/').'/'.$upcoProg[0]['link_daftar'].'/'.'35' }} onclick="secureIDUpcoming(${val.id})">
                            <img src="{{ asset('storage/images/programs/') }}/${val.link_pamflet}" loading="lazy" alt="${val.nama_program}" class="img-prog-180 border-gold-mis mx-2 mx-md-4 my-2">
                        </a>
                    </div>
                </div>
                `
            })

            let btnNextprev = `
            <button id="btnPrevProgram" class="btn-prevNext mx-2" onclick="prevProgram(${data.next})"><i class="mr-2 fas fa-chevron-left"></i> Prev</button>
            <button id="btnNextProgram" class="btn-prevNext" onclick="nextProgram(${data.next})">Next <i class="ml-2 fas fa-chevron-right"></i></button>
            `
            $("#btnNextProgram").removeClass('wait');
            $("#btnNextProgram").attr('disabled', false);

            if(data.ttlPastProg < page+12) {
                btnNextprev = `
                <button id="btnPrevProgram" class="btn-prevNext mx-2" onclick="prevProgram(${data.next})"><i class="mr-2 fas fa-chevron-left"></i> Prev</button>
                <button id="btnNextProgram" class="btn-prevNext" onclick="nextProgram(${data.next})" disabled>Next <i class="ml-2 fas fa-chevron-right"></i></button>
                `
            }

            $('#loc-data').html(dtPast)
            $('#loc-btn-program').html(btnNextprev)
            $('#loader').removeClass('d-flex')
            $('#loader').addClass('d-none')
        },
        error: function(err) {
            console.error(err)
        }
    })
  }

  //BUTTON PREVIOUS PAST PROGRAM
  function prevProgram(prevPage) {
    let page = parseInt(prevPage)-12
    $.ajax({
        type: "get",
        url: `{{ url('/home/pastprogram/${page}') }}`,
        beforeSend: function() {
                $("#loader").addClass('d-flex');
                $("#loader").removeClass('d-none');
                $("#btnPrevProgram").addClass('wait');
                $("#btnPrevProgram").attr('disabled', true);
            },
        success: function(data) {
            //console.log(data)
            let dtPast = ''
            $.each(data.pastProg, (index, val) => {
                dtPast +=
                `
                <div class="card-prog border-none">
                    <div class="custom-card">
                        <span class="roboto-condensed">${val.nama_program}</span>
                        <a href="{{ url('/program/daftar').'/'.$upcoProg[0]['link_daftar'].'/'.'35' }}" target="_blank">
                            <img src="{{ asset('storage/images/programs/') }}/${val.link_pamflet}" loading="lazy" alt="${val.nama_program}" class="img-prog-180 border-gold-mis mx-2 mx-md-4 my-2">
                        </a>
                    </div>
                </div>
                `
            })

            let btnNextprev = `
            <button id="btnPrevProgram" class="btn-prevNext mx-2" onclick="prevProgram(${data.next})"><i class="mr-2 fas fa-chevron-left"></i> Prev</button>
            <button id="btnNextProgram" class="btn-prevNext" onclick="nextProgram(${data.next})">Next <i class="ml-2 fas fa-chevron-right"></i></button>
            `
            $("#btnPrevProgram").removeClass('wait');
            $("#btnPrevProgram").attr('disabled', false);
            if(page == 0) {
                btnNextprev = `
                <button id="btnPrevProgram" class="btn-prevNext mx-2" onclick="prevProgram(${data.next})" disabled><i class="mr-2 fas fa-chevron-left"></i> Prev</button>
                <button id="btnNextProgram" class="btn-prevNext" onclick="nextProgram(${data.next})">Next <i class="ml-2 fas fa-chevron-right"></i></button>
                `
            }
            $('#loc-data').html(dtPast)
            $('#loc-btn-program').html(btnNextprev)
            $('#loader').removeClass('d-flex')
            $('#loader').addClass('d-none')
        },
        error: function(err) {
            console.error(err)
        }
    })
  }


</script>
@endsection
