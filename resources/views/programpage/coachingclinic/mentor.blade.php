@extends('template_merintis.merintis')

@section('head-title', 'Coaching Clinic | Merintis Indonesia')

@section('meta-description')
<meta name="description" content="Merintis Indonesia - Merintis Indonesia adalah ekosistem kreatif muda/i daerah untuk saling terhubung, berkolaborasi, dan melahirkan bisnis-bisnis yang inovatif, solutif dan aplikatif dari proses hulu ke hilir.">
@endsection

@section('addCSS')
<link rel="stylesheet" href="{{ asset('assets/css/home.css') }}">
@endsection

<?php
  $isLogin = false;
  $idAkun = '';
  if (Auth::check()) {
    $isLogin = true;
  } else if (Auth::viaRemember()) {
    $isLogin = true;
  }
?>

@section('header')
<div class="mt-3">
  <header>
     {{-- BATAS NAVBAR  --}}
    <nav class="navbar navbar-expand-sm navbar-dark">
      <div class="mx-auto">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMis" aria-controls="navbarMis" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse" id="navbarMis">
        <ul class="navbar-nav mx-auto fixed">
          <li class="nav-item">
            <a class="nav-link" href="/#beranda">Beranda</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#tentang">Tentang</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#program">Program</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#team">Team</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/#content">Content</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://info-merintisindonesia.medium.com/">Blog</a>
          </li>
          <li class="nav-item">
             {{-- Login/Logout  --}}
            <div id="link-log">
              <?= ($isLogin ? '<a class="nav-link" href="javascript:void(0)" onclick="logout()">Sign Out</a>' : '<a class="nav-link" id="btnSignIn" href="'.url('/signin').'">Sign In</a>'); ?>
            </div>
             {{-- End Login/Logout  --}}
          </li>
        </ul>
      </div>
    </nav>
  </header>
  {{-- ================ END Header Menu Area ================= --}}
  @endsection

  @section('content')
  {{-- ================ FIXED ALERT ================= --}}
  <div class='fixed-alert'></div>
  {{-- ================ END FIXED ALERT ================= --}}

</div>
{{-- ============ SECTION MENTOR --}}
<section id="program" class="mt-4">
  <div class="container roboto-condensed text-center">

    {{-- VIP COACH  --}}
    <h2 class="text-green mt-64 roboto-condensed">Find Your Coach, Sedulur!</h2>
    <div>
      <input type="text" id="mySearch" class="col-md-4 mt-5 ml-5 d-flex flex-wrap" onkeyup="getSearch()" placeholder="Silahkan cari nama atau bidang mentor, eg: marketing...">
    </div>
      <div class="d-flex flex-wrap justify-content-center mt-3">
        {{-- BARIS 1  --}}
        <div class="card border-none mx-2">
          <a href="https://www.linkedin.com/in/nisrinaakalusyamoktika/"><img src="{{ asset('assets/img/mentor/Nisrina A.png') }}" alt="vip1" class="img-180 my-2"></a>
          <div class="text-center mx-2">
            <span class="name-mentor block text-green text-18">Nisrina A</span>
            <span class="block text-gold text-12">Associate Consultant Glints </span>
            <span class="block text-gold text-12">Indonesia</span>
            <span class="speciality-mentor block roboto text-grey text-10">Human Resources</span>
          </div>
        </div>
        <div class="card border-none mx-2">
          <a href="https://www.linkedin.com/in/emmysurya/"><img src="{{ asset('assets/img/mentor/Emmy Surya.png') }}" alt="vip2" class="img-180 my-2"></a>
          <div class="text-center mx-2">
            <span class="name-mentor block text-green text-18">Emmy Surya</span>
            <span class="block text-gold text-12">Co-Founder Femalepreneur</span>
            <span class="block text-gold text-12">Indonesia</span>
            <span class="speciality-mentor block roboto text-grey text-10">Marketing</span>
          </div>
        </div>
        <div class="card border-none mx-2">
          <a href="https://www.linkedin.com/in/geraldine-christina-30024022/"><img src="{{ asset('assets/img/mentor/Geraldine C.png') }}" alt="vip3" class="img-180 my-2"></a>
          <div class="text-center mx-2">
            <span class="name-mentor block text-green text-18">Geraldine Christina</span>
            <span class="block text-gold text-12">Co-Founder Shestarts Indonesia</span>
            <span class="speciality-mentor block roboto text-grey text-10">Marketing</span>
          </div>
        </div>
        <div class="card border-none mx-2">
          <a href="https://www.linkedin.com/in/bung-anwar-655308200/"><img src="{{ asset('assets/img/mentor/Asmawi Anwar.png') }}" alt="vip4" class="img-180 my-2"></a>
          <div class="text-center mx-2">
            <span class="name-mentor block text-green text-18">Asmawi Anwar</span>
            <span class="block text-gold text-12">Founder Meet & Co Space</span>
            <span class="speciality-mentor block roboto text-grey text-10">Branding</span>
          </div>
        </div>
        <div class="card border-none mx-2">
          <a href="https://www.linkedin.com/in/bobby-wibowo-6b0065200/"><img src="{{ asset('assets/img/mentor/Bobby Wibowo.png') }}" alt="vip5" class="img-180 my-2"></a>
          <div class="text-center mx-2">
            <span class="name-mentor block text-green text-18">Bobby Wibowo</span>
            <span class="block text-gold text-12">CEO Maesa Group Holding</span>
            <span class="block text-gold text-12">Company </span>
            <span class="speciality-mentor block roboto text-grey text-10">Legal</span>
          </div>
        </div>

        {{-- DISEMBUNYIKAN  --}}
          {{-- BARIS 2 --}}
          <div class="card border-none collapse hide-vip mx-2">
            <a href="https://www.linkedin.com/in/fauzi-ghozali/"><img src="{{ asset('assets/img/mentor/Fauzi Ghozali.png') }}" alt="vip15" class="img-180 my-2"></a>
            <div class="text-center mx-2">
              <span class="name-mentor block text-green text-18">Fauzi Ghozali</span>
              <span class="block text-gold text-12">Project Manager Maesa Group</span>
              <span class="block text-gold text-12">Holding Company </span>
              <span class="speciality-mentor block roboto text-grey text-10">Teknologi</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip mx-2">
            <a href="https://www.linkedin.com/in/anikprimawati/"><img src="{{ asset('assets/img/mentor/Anik Primawati.png') }}" alt="vip16" class="img-180 my-2"></a>
            <div class="text-center mx-2">
              <span class="name-mentor block text-green text-18">Anik Primawati</span>
              <span class="block text-gold text-12">CFO Maesa Group Holding</span>
              <span class="block text-gold text-12">Company</span>
              <span class="speciality-mentor block roboto text-grey text-10">Finance</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip mx-2">
            <a href="https://www.linkedin.com/in/titikrusmiati/"><img src="{{ asset('assets/img/mentor/Titik Rusmiati.png') }}" alt="vip17" class="img-180 my-2"></a>
            <div class="text-center mx-2">
              <span class="name-mentor block text-green text-18">Titik Rusmiati</span>
              <span class="block text-gold text-12">Co-Founder Femalepreneur</span>
              <span class="block text-gold text-12">Indonesia</span>
              <span class="speciality-mentor block roboto text-grey text-10">Social Impact</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip mx-2">
            <a href="https://www.linkedin.com/in/ranityanurlita/"><img src="{{ asset('assets/img/mentor/Ranitya Nurlita.png') }}" alt="vip18" class="img-180 my-2"></a>
            <div class="text-center mx-2">
              <span class="name-mentor block text-green text-18">Ranitya Nurlita</span>
              <span class="block text-gold text-12">Founder Wastehub.id</span>
              <span class="speciality-mentor block roboto text-grey text-10">Social Impact</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip mx-2">
            <a href="https://www.linkedin.com/in/nadia-sarah-5900101b/"><img src="{{ asset('assets/img/mentor/Nadia Sarah.png') }}" alt="vip19" class="img-180 my-2"></a>
            <div class="text-center mx-2">
              <span class="name-mentor block text-green text-18">Nadia Sarah</span>
              <span class="block text-gold text-12">Co-Founder Shestarts Indonesia</span>
              <span class="speciality-mentor block roboto text-grey text-10">Business Development</span>
            </div>
          </div>
          <div class="card border-none collapse hide-vip mx-2">
            <a href="https://www.linkedin.com/in/azmialisani/"><img src="{{ asset('assets/img/mentor/M Azmi Ali.png') }}" alt="vip20" class="img-180 my-2"></a>
            <div class="text-center mx-2">
              <span class="name-mentor block text-green text-18">Azmi Ali Sani</span>
              <span class="block text-gold text-12">Founder Biz Lab</span>
              <span class="speciality-mentor block roboto text-grey text-10">Business Development</span>
            </div>
          </div>
          {{-- END DISEMBUNYIKAN  --}}

    </div>
    {{-- BUTTON_SELENGKAPNYA --}}
    <button type="button" class="btn button-kuning border-none roboto-condensed let-space-08 my-4" data-toggle="collapse" data-target=".hide-vip" id="btnMore">Selengkapnya</button>
</section>
{{-- ================ END Section Area ================= --}}
@endsection

@section('footer')
{{-- ================ Footer Area ================= --}}
<footer class="roboto py-2">
  <div class="container text-center">
    <span class="text-grey">Copyright &copy; <span id="yearNow">tahun</span> All rights reserved</span>
  </div>
</footer>
{{-- ================ END Footer Area ================= --}}
@endsection

@section('addScript')

<script>
// ================== SEARCHING ====================
  // this function trigger to filtering
  function getSearch() {
    let input, filter, listMentor

    input = document.getElementById("mySearch")
    filter = input.value.toLowerCase()

    listMentor = document.getElementsByClassName("card")

    for (let i = 0 ; i < listMentor.length ; i++) {
      let name = listMentor[i].getElementsByClassName("name-mentor")[0].innerText
      let speciality = listMentor[i].getElementsByClassName("speciality-mentor")[0].innerText

        if (filter.length == 1) {
            document.getElementById("btnMore").click()
            document.getElementById("btnMore").style.visibility = "hidden"
        } else if (filter.length == 0){
            document.getElementById("btnMore").style.visibility = "visible"
        }

      if (name.toLowerCase().indexOf(filter) > -1 || speciality.toLowerCase().indexOf(filter) > -1) {
          listMentor[i].style.display = "";
        } else {
          listMentor[i].style.display = "none"
        }
      }
  }

  // Tambahan Script

  // ----------- CEK JIKA DARI HALAMAN DAFTAR -----------
  $(document).ready(function() {
    let params = (new URL(document.location)).searchParams;
    let stat = parseInt(params.get('stat'));
    if (stat) {
      $("#modalLogin").modal('toggle');
    }
  });
  // ----------- END CEK JIKA DARI HALAMAN DAFTAR -----------

  // SHOW NAVBAR FIXED
  window.onscroll = changeNav;

  function changeNav() {
    let navbar = $('nav');
    if (window.pageYOffset > 90) {
      navbar.addClass('navbar-fixed')
    } else {
      navbar.removeClass('navbar-fixed');
    }
  }

  // GET YEAR NOW
  let date = new Date();
  let now = date.getFullYear();
  document.getElementById("yearNow").innerHTML = now;

    // ==================== LOGOUT ===================
  function logout() {
    //console.log("Berhasil logout");
    // --------------- LOGOUT -------------------
    $(this).on("click", function() {
      // ------ MENGHAPUS SESSION DI DATABASE
      $('#link-log').addClass("active");
      $.ajax({
        type: "post",
        url: "{{ url('/akun/hapussession') }}",
        data: {"_token": "{{ csrf_token() }}" },
        success: function() {
          window.location.replace("{{ url('/') }}");
        },
        error: function(err) {
          // console.error(err)
        }
      })
      // ------ END MENGHAPUS SESSION DI DATABASE
    })
  }
  // ==================== END LOGOUT ===============

  // ------------------ FIXED ALERT
  function alFixedBerhasil(pesan) {
    let alBerhasil = `
      <div class="alert alert-success alert-dismissible fade show" role="alert">
      <div id="text-berhasil">${pesan}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>
      `
    $('.fixed-alert').html(alBerhasil)
    $('.fixed-alert').html(alBerhasil).animate({
      right: '12px'
    })

    $('.fixed-alert .close').on("click", function() {
      $('.fixed-alert').animate({
        right: '-100px'
      });
      $('.fixed-alert .alert').removeClass('.show');
    })
  }

  function alFixedGagal(pesan) {
    let alGagal = `
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <div id="text-berhasil">${pesan}</div>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>
      `
    $('.fixed-alert').html(alGagal).animate({
      right: '12px'
    })

    $('.fixed-alert .close').on("click", function() {
      $('.fixed-alert').animate({
        right: '-100px'
      });
      $('.fixed-alert .alert').removeClass('.show');
    })
  }
  // -------------------------- END FIXED ALERT

  // ------------------ END BTN COLLAPSE VIP ----------------

  // SHOW MESSAGE SUCCESS LOGIN
      if(sessionStorage.getItem("login") === "berhasil") {
        alFixedBerhasil('Login Berhasil')
        sessionStorage.removeItem("login")
      }
  // END SHOW MESSAGE SUCCESS LOGIN

    // ========== BTN SIGNIN
  $('#btnSignIn').on("click", function() {
    $('#link-log').addClass("active");
  })
  // ========== END BTN SIGNIN
  
  // === AMBIL PARAMETER
  const urlParams = new URLSearchParams(window.location.search)
  const params = urlParams.get('program')
  //console.log(params)
  if (params === 'upcoming') {
    window.location.hash = 'program'
    $('#past-program').removeClass('menu-active')
    $('#ongoing-program').removeClass('menu-active')
    $('#upcoming-program').addClass('menu-active')
    getUpcomProg()
  } else {
    getPastProg()
  }
  // === END AMBIL PARAMETER

  // SECURE DETAIL ONGOING
  function secureIDProgram(id) {
    let isLogin = '<?= ($isLogin ? $isLogin: 'false') ?>'
    if(isLogin === 'false') {
      alFixedGagal('Silahkan sign in dahulu!')
    } else {
      window.location.href = `{{ url('/program/detail/ongoing') }}/${id}`
    }
  }

  // SECURE DETAIL UPCOMING
  function secureIDUpcoming(id) {
    let isLogin = '<?= ($isLogin ? $isLogin: 'false') ?>'
    if(isLogin === 'false') {
      alFixedGagal('Silahkan sign in dahulu!')
    } else {
      window.location.href = `{{ url('/program/detail/upcoming') }}/${id}`
    }
  }

  //BUTTON NEXT PAST PROGRAM
  function nextProgram(nextPage) {
    let page = parseInt(nextPage)+12
    $.ajax({
        type: "get",
        url: `{{ url('/home/pastprogram/${page}') }}`,
        beforeSend: function() {
                $("#loader").addClass('d-flex');
                $("#loader").removeClass('d-none');
                $("#btnNextProgram").addClass('wait');
                $("#btnNextProgram").attr('disabled', true);
            },
        success: function(data) {
            //console.log(data)
            let dtPast = ''
            $.each(data.pastProg, (index, val) => {
                dtPast +=
                `
                <div class="card-prog border-none">
                    <div class="custom-card">
                        <span class="roboto-condensed">${val.nama_program}</span>
                        <a href="{{ url('/program/daftar/coachclinic/miniclass') }}" target="_blank">
                            <img src="{{ asset('storage/images/programs/') }}/${val.link_pamflet}" loading="lazy" alt="${val.nama_program}" class="img-prog-180 border-gold-mis mx-2 mx-md-4 my-2">
                        </a>
                    </div>
                </div>
                `
            })

            let btnNextprev = `
            <button id="btnPrevProgram" class="btn-prevNext mx-2" onclick="prevProgram(${data.next})"><i class="mr-2 fas fa-chevron-left"></i> Prev</button>
            <button id="btnNextProgram" class="btn-prevNext" onclick="nextProgram(${data.next})">Next <i class="ml-2 fas fa-chevron-right"></i></button>
            `
            $("#btnNextProgram").removeClass('wait');
            $("#btnNextProgram").attr('disabled', false);

            if(data.ttlPastProg < page+12) {
                btnNextprev = `
                <button id="btnPrevProgram" class="btn-prevNext mx-2" onclick="prevProgram(${data.next})"><i class="mr-2 fas fa-chevron-left"></i> Prev</button>
                <button id="btnNextProgram" class="btn-prevNext" onclick="nextProgram(${data.next})" disabled>Next <i class="ml-2 fas fa-chevron-right"></i></button>
                `
            }

            $('#loc-data').html(dtPast)
            $('#loc-btn-program').html(btnNextprev)
            $('#loader').removeClass('d-flex')
            $('#loader').addClass('d-none')
        },
        error: function(err) {
            console.error(err)
        }
    })
  }

  //BUTTON PREVIOUS PAST PROGRAM
  function prevProgram(prevPage) {
    let page = parseInt(prevPage)-12
    $.ajax({
        type: "get",
        url: `{{ url('/home/pastprogram/${page}') }}`,
        beforeSend: function() {
                $("#loader").addClass('d-flex');
                $("#loader").removeClass('d-none');
                $("#btnPrevProgram").addClass('wait');
                $("#btnPrevProgram").attr('disabled', true);
            },
        success: function(data) {
            //console.log(data)
            let dtPast = ''
            $.each(data.pastProg, (index, val) => {
                dtPast +=
                `
                <div class="card-prog border-none">
                    <div class="custom-card">
                        <span class="roboto-condensed">${val.nama_program}</span>
                        <a href="{{ url('/program/daftar/coachclinic/miniclass') }}" target="_blank">
                            <img src="{{ asset('storage/images/programs/') }}/${val.link_pamflet}" loading="lazy" alt="${val.nama_program}" class="img-prog-180 border-gold-mis mx-2 mx-md-4 my-2">
                        </a>
                    </div>
                </div>
                `
            })

            let btnNextprev = `
            <button id="btnPrevProgram" class="btn-prevNext mx-2" onclick="prevProgram(${data.next})"><i class="mr-2 fas fa-chevron-left"></i> Prev</button>
            <button id="btnNextProgram" class="btn-prevNext" onclick="nextProgram(${data.next})">Next <i class="ml-2 fas fa-chevron-right"></i></button>
            `
            $("#btnPrevProgram").removeClass('wait');
            $("#btnPrevProgram").attr('disabled', false);
            if(page == 0) {
                btnNextprev = `
                <button id="btnPrevProgram" class="btn-prevNext mx-2" onclick="prevProgram(${data.next})" disabled><i class="mr-2 fas fa-chevron-left"></i> Prev</button>
                <button id="btnNextProgram" class="btn-prevNext" onclick="nextProgram(${data.next})">Next <i class="ml-2 fas fa-chevron-right"></i></button>
                `
            }
            $('#loc-data').html(dtPast)
            $('#loc-btn-program').html(btnNextprev)
            $('#loader').removeClass('d-flex')
            $('#loader').addClass('d-none')
        },
        error: function(err) {
            console.error(err)
        }
    })
  }


</script>
@endsection
